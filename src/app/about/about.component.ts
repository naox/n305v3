import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { N305RestService } from '../n305-rest.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  title: string;
  information: string;
  hasAlreadyLoaded: boolean;
  templateRef: any;

  constructor(private appService: AppService, private n305RestService: N305RestService) {
    this.setHasAlreadyLoaded(this.appService.aboutHasAlreadyLoaded);
  }

  async ngOnInit(): Promise<void> {
    let data = await this.n305RestService.constants();
    if (data) {
      this.title = data['aboutDomConstants']['title'];
      this.information = data['aboutDomConstants']['aboutText'];
    }
  }

  /**
  * Method determines whether to navigate to new location 
  * or open a new browser window. 
  * @param url string containing the new location.
  */
  changeLocation(url: string): void {
    if (url.includes('mailto')) {
      window.location.href = url;
    } else {
      window.open(url, 'n305_3rd_window', 'n305_3rd_window, height=600px, width = 600px');
    }
  }

  /**
  * Method sets component properties based on state of load.
  * or open a new browser window. 
  * @param value boolean determing whether or not load state is completed.
  */
  setHasAlreadyLoaded(value: boolean): void {
    this.appService.aboutHasAlreadyLoaded = value;
    this.hasAlreadyLoaded = value;
  }

  /**
  * Method sets component properties for skeleton loader.
  */
  hideSkeletonLoaders(): void {
    if (this.hasAlreadyLoaded === false) {
      setTimeout(() => {
        this.setHasAlreadyLoaded(true);
      }, this.appService.skeletonLoaderDelay);
    }
  }

  /**
  * Method sets the new template reference and adds an event listener.
  * @param value containing the new template reference.
  */
  attachToTempRef(value: any): void {
    if (!this.templateRef) {
      this.templateRef = value;
      value.addEventListener('load', () => this.hideSkeletonLoaders());
    }
  }
}
