import { IMock, It, Mock, Times } from 'typemoq';
import { N305RestService } from '../n305-rest.service';
import { AppService } from '../app.service';
import { AboutComponent } from './about.component';
import { HttpClient } from '@angular/common/http';

let httpClientMock: IMock<HttpClient>;
let aboutComponent: AboutComponent;
let n305RestService: N305RestService;
let appService: AppService;

beforeEach((() => {
    httpClientMock = Mock.ofType<HttpClient>();
    appService = new AppService();
    httpClientMock.setup(m => m.get(It.isAny())).verifiable;
    n305RestService = new N305RestService(httpClientMock.object);
    aboutComponent = new AboutComponent(appService, n305RestService);
}));

describe('AboutComponent', () => {
    it('should create', () => {
        expect(aboutComponent).toBeTruthy();
    });

    it('should call HttpClient.get once', async () => {
        await aboutComponent.ngOnInit();
        httpClientMock.verify(m => m.get(It.isAny()), Times.once());
        expect().nothing();
    });
});
