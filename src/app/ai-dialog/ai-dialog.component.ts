import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DialogData } from '../models/ai-dialog.model';

@Component({
  selector: 'app-ai-dialog',
  templateUrl: './ai-dialog.component.html',
  styleUrl: './ai-dialog.component.scss'
})
export class AiDialogComponent {
  constructor(public dialogRef: MatDialogRef<AiDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
