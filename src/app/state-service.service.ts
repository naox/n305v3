import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AiAnswer } from './models/ai-answer.model';
import { AiProvider } from './models/provider.model';

@Injectable({
  providedIn: 'root'
})
export class StateServiceService {
  private answers = new BehaviorSubject<AiAnswer[]>([]);
  private providers = new BehaviorSubject<AiProvider[]>([]);
  private scrollPosition = new BehaviorSubject<number>(0);
  constructor() { }

  get answers$(): Observable<AiAnswer[]> {
    return this.answers.asObservable();
  }

  get providers$(): Observable<AiProvider[]> {
    return this.providers.asObservable();
  }

  get scrollPosition$(): Observable<number> {
    return this.scrollPosition.asObservable();
  }

  saveAnswers(answers: AiAnswer[]): void {
    this.answers.next(answers);
  }

  saveProviders(providers: AiProvider[]): void {
    this.providers.next(providers);
  }

  saveScrollPosition(scrollPosition: number): void {
    this.scrollPosition.next(scrollPosition);
  }

  clearState(): void {
    this.answers.next([]);
    this.scrollPosition.next(0);
  }
}
