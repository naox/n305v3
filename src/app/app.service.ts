import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  homeHasAlreadyLoaded:boolean;
  galleryHasAlreadyLoaded:boolean;
  aboutHasAlreadyLoaded:boolean;
  skeletonLoaderDelay:number = 1000;
  constructor() {
    this.homeHasAlreadyLoaded = false;
    this.galleryHasAlreadyLoaded = false;
    this.aboutHasAlreadyLoaded = false;
   }
}
