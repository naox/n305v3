import { SafePipe } from './safe.pipe';
import { DomSanitizer} from '@angular/platform-browser';
import {IMock, Mock} from 'typemoq';

let domSanitizerMock: IMock<DomSanitizer>;
domSanitizerMock = Mock.ofType<DomSanitizer>();
describe('SafePipe', () => {
  it('create an instance', () => {
    const pipe = new SafePipe(domSanitizerMock.object);
    expect(pipe).toBeTruthy();
  });
});
