import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
const N305_API_URL: string = 'https://n305.azurewebsites.net/ai/';

@Injectable({
  providedIn: 'root'
})
export class N305RestService {
  constructor(private httpClient: HttpClient) { }

  /**
  * Method that returns dependencies.json via HTTP call.
  */
  public async packageJson(): Promise<any> {
    let data = await this.httpClient.get('dependencies.json');
    if (data) {
      return data.toPromise();
    } else {
      return null;
    }
  }

  /**
  * Method that returns constants.json via HTTP call.
  */
  public async constants(): Promise<any> {
    let data = await this.httpClient.get('constants.json');
    if (data) {
      return data.toPromise();
    } else {
      return null;
    }
  }

  /**
  * The method that calls the answer endpoint in n305Api
  * @param question 
  * @returns A string containing the answer to a question.
  */
  public async aiAnswer(question: string, provider: string): Promise<any> {
    let url = `${N305_API_URL}answer/?provider=${provider}&question=${question}`;
    let data = await this.httpClient.get(url, { responseType: 'text' });
    if (data) {
      return data.toPromise();
    } else {
      return null;
    }
  }

    /**
  * The method that get list of providers
  * @param question 
  * @returns A string containing the answer to a question.
  */
    public async providers(): Promise<any> {
      let url = `${N305_API_URL}providers`;
      let data = await this.httpClient.get(url, { responseType: 'json' });
      if (data) {
        return data.toPromise();
      } else {
        return null;
      }
    }
}