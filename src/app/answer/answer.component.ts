import { Component, Input } from '@angular/core';
import { AiAnswer } from '../models/ai-answer.model';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrl: './answer.component.scss'
})
export class AnswerComponent {
  @Input() aiAnswer: AiAnswer;
}
