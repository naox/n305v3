import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {
  /**
  * Method that returns a sorted array in ascending or descending order.
  * @param array an array that will be sorted.
  * @param order string that respesents the sorting order of array. 
  */
  transform(array: Array<any>, order: string): Array<any> {
    if (array && array.sort) {
      array.sort((a: any, b: any) => {
        a = a.htmlItem.value; b = b.htmlItem.value;
        if (order === 'asc') {
          if (a < b) {
            return -1;
          } else if (a > b) {
            return 1;
          } else {
            return 0;
          }
        } else {
          if (a > b) {
            return -1;
          } else if (a < b) {
            return 1;
          } else {
            return 0;
          }
        }
      });
      return array;
    }
  }
}
