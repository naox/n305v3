import { IMock, Mock } from 'typemoq';
import { HttpClient } from '@angular/common/http';
import { N305RestService } from './n305-rest.service';
import { Observable } from 'rxjs';

let httpClientMock: IMock<HttpClient>;
let n305RestService: N305RestService;
let httpGetFake: any;

beforeEach((() => {
  httpClientMock = Mock.ofType<HttpClient>();
  n305RestService = new N305RestService(httpClientMock.object);
  httpGetFake = (): Observable<JSON> => {
    return null;
  }
}));

describe('N305RestService', () => {

  it('should be created', () => {
    expect(n305RestService).toBeTruthy();
  });

  it('should implement constants method of type Observable<JSON>', () => {
    expect(n305RestService.constants).toBeTruthy();
    expect(typeof n305RestService.constants === typeof httpGetFake);
  });

  it('should implement packageJson method of type Observable<JSON>', () => {
    expect(n305RestService.packageJson).toBeTruthy();
    expect(typeof n305RestService.packageJson === typeof httpGetFake);
  });
});
