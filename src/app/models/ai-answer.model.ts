export interface AiAnswer {
    prompt: string,
    answer: string;
    provider: string;
}