import { Component, OnInit } from '@angular/core';
import { N305RestService } from '../n305-rest.service';

@Component({
  selector: 'app-dependencies',
  templateUrl: './dependencies.component.html',
  styleUrls: ['./dependencies.component.scss']
})
export class DependenciesComponent implements OnInit {
  private depList: string[];
  formatedDepList: any[] = [];

  constructor(private n305RestService: N305RestService) { }

  async ngOnInit(): Promise<void> {
    await this.getDependencies();
  }

  /**
  * Method converts JSON from packageJson into readable format. 
  */
  private async getDependencies(): Promise<void> {
    let data = await this.n305RestService.packageJson();
    if (data) {
      let depString = JSON.stringify(data).replace('{', '').replace('}', '');
      this.depList = depString.split(',');

      this.depList.forEach(datum => {
        let properties = datum.split(':');
        this.formatedDepList.push({ 'name': properties[0].replace('"', '').replace('"', ''), 'version': properties[1].replace('"', '').replace('"', '') });
      });
    }
  }
}
