import { IMock, It, Mock, Times } from 'typemoq';
import { N305RestService } from '../n305-rest.service';
import { DependenciesComponent } from './dependencies.component';
import { HttpClient } from '@angular/common/http';

let httpClientMock: IMock<HttpClient>;
let dependenciesComponent: DependenciesComponent;
let n305RestService: N305RestService;

beforeEach((() => {
    httpClientMock = Mock.ofType<HttpClient>();
    httpClientMock.setup(m => m.get(It.isAny())).verifiable;
    n305RestService = new N305RestService(httpClientMock.object);
    dependenciesComponent = new DependenciesComponent(n305RestService);
}));

describe('DependenciesComponent', () => {
    it('should create', () => {
        expect(dependenciesComponent).toBeTruthy();
    });
    
    it('should call HttpClient.get once', async () => {
        await dependenciesComponent.ngOnInit();
        httpClientMock.verify(m => m.get(It.isAny()), Times.once());
        expect().nothing();
    });
});

