import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AppInterceptor implements HttpInterceptor {
    constructor() { }

    /**
    * Method that intercepts all HTTP requests.
    * @param req HttpRequest to intercept.
    * @param next HttpHandler for next state of HttpRequest. 
    */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authReq = req.clone({
            //Clone the request to add the new header.
        });
        return next.handle(authReq).pipe(catchError((err: any) => {
            return throwError(err);
        }));
    }
}