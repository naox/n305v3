import { Component, OnInit } from '@angular/core';
import { N305RestService } from '../n305-rest.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-user-main',
  templateUrl: './user-main.component.html',
  styleUrls: ['./user-main.component.scss'],
  animations: [
    trigger('openClose', [
      state('true', style({
        opacity: 0.4
      })),
      state('false', style({
        opacity: 1
      })),
      transition('false <=> true', animate('1s'))
    ])
  ]
})

export class UserMainComponent implements OnInit {
  title: string;
  topResults: string;
  menuSelection: string = 'ai';
  sortOrder: string = '';
  opened: boolean = false;
  currentView: string = 'Ai';
  constructor(public dialog: MatDialog, private n305RestService: N305RestService) { }

  async ngOnInit(): Promise<void> {
    let data = await this.n305RestService.constants();
    if (data) {
      this.title = data['appDomConstants']['title'];
      this.topResults = data['appDomConstants']['topResults'];;
    }
  }

  /**
  * The method that navigates to new location.
  * @param url string containing the new location.
  */
  changeLocation(url: string) {
    window.location.href = url;
  }

  /**
  * The method that sets component display properties based on menu selection.
  * @param view string containing the selected menu option.
  * @param name string containing name of current view. 
  */
  navTo(view: string, name: string) {
    this.menuSelection = view;
    this.opened = false;
    this.currentView = name;
  }

  /**
   * The method that reloads app including cache busting.
   */
  refresh(): void {
    let dialogRef = this.dialog.open(ConfirmDialogComponent, null);
    dialogRef.afterClosed().subscribe(refresh => {
      if (refresh) {
        this.changeLocation(`https://n305.net?=${(Math.floor(Math.random() * 1000000000000)).toString()}`);
      }
    });
  }
}
