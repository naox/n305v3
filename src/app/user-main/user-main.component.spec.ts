import { IMock, It, Mock, Times } from 'typemoq';
import { N305RestService } from '../n305-rest.service';
import { UserMainComponent } from './user-main.component';
import { HttpClient } from '@angular/common/http';

let httpClientMock: IMock<HttpClient>;
let userComponent: UserMainComponent;
let n305RestService: N305RestService;

beforeEach((() => {
    httpClientMock = Mock.ofType<HttpClient>();
    httpClientMock.setup(m => m.get(It.isAny())).verifiable;
    n305RestService = new N305RestService(httpClientMock.object);
    userComponent = new UserMainComponent(n305RestService);
}));

describe('UserMainComponent', () => {
    it('should create', () => {
        expect(userComponent).toBeTruthy();
    });

    it('should call HttpClient.get once', async () => {
        await userComponent.ngOnInit();
        httpClientMock.verify(m => m.get(It.isAny()), Times.once());
        expect().nothing();
    });
});
