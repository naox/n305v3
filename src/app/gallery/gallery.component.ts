import { Component, OnInit } from '@angular/core';
import { N305RestService } from '../n305-rest.service';
import { AppService } from '../app.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  hasAlreadyLoaded: boolean;
  galleryItems: any[];
  templateRef: any;

  constructor(private appService: AppService, private n305RestService: N305RestService) {
    this.setHasAlreadyLoaded(this.appService.galleryHasAlreadyLoaded);
  }

  async ngOnInit(): Promise<void> {
    let data = await this.n305RestService.constants();
    if (data) {
      this.galleryItems = data['galleryDomConstants'];
    }
  }

  /**
  * Method opens a new browser window.
  * @param url string containing the new location.
  */
  changeLocation(url: string): void {
    window.open(url, 'n305_2nd_window', 'n305_2nd_window, height=600px, width = 600px');
  }

  /**
  * Method sets component properties based on state of load.
  * or open a new browser window. 
  * @param value boolean determing whether or not load state is completed.
  */
  setHasAlreadyLoaded(value: boolean): void {
    this.appService.galleryHasAlreadyLoaded = value;
    this.hasAlreadyLoaded = value;
  }

  /**
  * Method sets component properties for skeleton loader.
  */
  hideSkeletonLoaders(): void {
    if (this.hasAlreadyLoaded === false) {
      setTimeout(() => {
        this.setHasAlreadyLoaded(true);
      }, this.appService.skeletonLoaderDelay);
    }
  }

  /**
  * Method sets the new template reference and adds an event listener.
  * @param value containing the new template reference.
  */
  attachToTempRef(value: any): void {
    if (!this.templateRef) {
      this.templateRef = value;
      value.addEventListener('load', () => this.hideSkeletonLoaders());
    }
  }
}
