import { IMock, It, Mock, Times } from 'typemoq';
import { N305RestService } from '../n305-rest.service';
import { AppService } from '../app.service';
import { HttpClient } from '@angular/common/http';
import { GalleryComponent } from './gallery.component';

let galleryComponent: GalleryComponent;
let appServiceMock: IMock<AppService>;
let httpClientMock: IMock<HttpClient>;
let n305RestService: N305RestService;

beforeEach((() => {
    appServiceMock = Mock.ofType<AppService>();
    httpClientMock = Mock.ofType<HttpClient>();
    httpClientMock.setup(m => m.get(It.isAny())).verifiable;
    n305RestService = new N305RestService(httpClientMock.object);
    galleryComponent = new GalleryComponent(appServiceMock.object, n305RestService);
}));

describe('GalleryComponent', () => {
    it('should create', () => {
        expect(galleryComponent).toBeTruthy();
    });

    it('should call HttpClient.get once', async () => {
        await galleryComponent.ngOnInit();
        httpClientMock.verify(m => m.get(It.isAny()), Times.once());
        expect().nothing();
    });
});

