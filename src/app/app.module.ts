import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AppInterceptor } from './app.interceptor';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDialogModule } from '@angular/material/dialog';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ClipboardModule } from '@angular/cdk/clipboard';

import { AppService } from './app.service';
import { N305RestService } from './n305-rest.service';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { SafePipe } from './safe.pipe';
import { SortPipe } from './sort.pipe';
import { UserMainComponent } from './user-main/user-main.component';
import { GalleryComponent } from './gallery/gallery.component';
import { DependenciesComponent } from './dependencies/dependencies.component';
import { AiComponent } from './ai/ai.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { AnswerComponent } from './answer/answer.component';

const appRoutes: Routes = [
  { path: '', component: UserMainComponent },
  { path: 'Home/PrivacyPolicy', component: PrivacypolicyComponent },
  { path: 'privacy-policy', component: PrivacypolicyComponent }
]

declare module "@angular/core" {
  interface ModuleWithProviders<T = any> {
    ngModule: Type<T>;
  }
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    SafePipe,
    SortPipe,
    UserMainComponent,
    GalleryComponent,
    DependenciesComponent,
    PrivacypolicyComponent,
    AiComponent,
    ConfirmDialogComponent,
    AnswerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    NgxSkeletonLoaderModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSidenavModule,
    MatDialogModule,
    MatIconModule,
    MatToolbarModule,
    ClipboardModule
  ],
  providers: [AppService, N305RestService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppInterceptor,
      multi: true
    }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
