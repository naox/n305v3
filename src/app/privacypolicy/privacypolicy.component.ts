import { Component } from '@angular/core';

@Component({
  selector: 'app-privacypolicy',
  templateUrl: './privacypolicy.component.html',
  styleUrls: ['./privacypolicy.component.scss']
})
export class PrivacypolicyComponent {
  constructor() { }

  /**
  * Method that navigates to new location.
  * @param url string containing the new location.
  */
  changeLocation(url: string): void {
    window.location.href = url;
  }
}
