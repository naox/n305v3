import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('onReady', [
      state('true', style({
        opacity: 1
      })),
      state('false', style({
        opacity: 0
      })),
      transition('false <=> true', animate('3s ease-out'))
    ])
  ]
})
export class AppComponent implements OnInit {
  loaded: boolean = false;

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      this.loaded = true;
    }, 10);
  }
}


