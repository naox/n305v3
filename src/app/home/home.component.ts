import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { N305RestService } from '../n305-rest.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  title: string;
  information: string;
  hasAlreadyLoaded: boolean;
  templateRef: any;

  constructor(private appService: AppService, private n305RestService: N305RestService) {
    this.setHasAlreadyLoaded(this.appService.homeHasAlreadyLoaded);
  }

  async ngOnInit(): Promise<void> {
    let data = await this.n305RestService.constants();
    if (data) {
      this.title = data['homeDomConstants']['title'];
      this.information = data['homeDomConstants']['homeText'];
    }
  }

  /**
  * Method determines whether to navigate to new location 
  * or open a new browser window. 
  * @param url string containing the new location.
  */
  changeLocation(url: string): void {
    if (url.includes('mailto')) {
      window.location.href = url;
    } else {
      window.open(url, 'n305_2nd_window', 'n305_2nd_window, height=600px, width = 600px');
    }
  }

  /**
  * Method sets component properties based on state of load.
  * or open a new browser window. 
  * @param value boolean determing whether or not load state is completed.
  */
  setHasAlreadyLoaded(value: boolean): void {
    this.appService.homeHasAlreadyLoaded = value;
    this.hasAlreadyLoaded = value;
  }

  /**
  * Method sets the new template reference and adds an event listener.
  * @param value containing the new template reference.
  */
  attachToTempRef(value: any): void {
    const _this = this;
    if (!this.templateRef) {
      this.templateRef = value;
      value.addEventListener("load", () => this.hideSkeletonLoaders(_this));
    }
  }

  /**
  * Method sets component properties for skeleton loader.
  */
  hideSkeletonLoaders(_this: any): void {
    if (_this.hasAlreadyLoaded !== true) {
      setTimeout(() => {
        _this.setHasAlreadyLoaded(true);
      }, _this.appService.skeletonLoaderDelay);
    }
  }
}
