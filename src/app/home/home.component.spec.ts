import { IMock, It, Mock, Times } from 'typemoq';
import { N305RestService } from '../n305-rest.service';
import { AppService } from '../app.service';
import { HomeComponent } from './home.component';
import { HttpClient } from '@angular/common/http';

let httpClientMock: IMock<HttpClient>;
let homeComponent: HomeComponent;
let n305RestService: N305RestService;
let appService: AppService;

beforeEach((() => {
    httpClientMock = Mock.ofType<HttpClient>();
    appService = new AppService();
    httpClientMock.setup(m => m.get(It.isAny())).verifiable;
    n305RestService = new N305RestService(httpClientMock.object);
    homeComponent = new HomeComponent(appService, n305RestService);
}));

describe('HomeComponent', () => {
    it('should create', () => {
        expect(homeComponent).toBeTruthy();
    });

    it('should call HttpClient.get once', async () => {
        await homeComponent.ngOnInit();
        httpClientMock.verify(m => m.get(It.isAny()), Times.once());
        expect().nothing();
    });
});

