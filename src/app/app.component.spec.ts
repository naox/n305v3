import { AppComponent } from './app.component';

let appComponent: AppComponent;

beforeEach((() => {
  appComponent = new AppComponent();
}));

describe('AppComponent', () => {
  it('should create', () => {
    expect(appComponent).toBeTruthy();
  });
});
