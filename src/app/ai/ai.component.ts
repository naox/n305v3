import { ChangeDetectorRef, Component, ElementRef, OnInit } from '@angular/core';
import { N305RestService } from '../n305-rest.service';
import { AiProvider } from '../models/provider.model';
import { AiAnswer } from '../models/ai-answer.model';
import { StateServiceService } from '../state-service.service';
import { markdownToTxt } from 'markdown-to-txt';

@Component({
  selector: 'app-ai',
  templateUrl: './ai.component.html',
  styleUrl: './ai.component.scss'
})
export class AiComponent {
  public placeHolder: string = `How can I help you?`;
  public aiReponse: string = '';
  public disableButton: boolean = false;
  public providers: AiProvider[];
  public selectedProvider: AiProvider;
  public aiAnswers: AiAnswer[] = [];
  public hasLoaded: boolean;
  private readonly answerElm: string = '.b-ai--answer';

  constructor(private elRef: ElementRef,
    private changeDetector: ChangeDetectorRef,
    private n305RestService: N305RestService,
    private stateServiceService: StateServiceService) { }

  async ngOnInit() {
    this.hasLoaded = false;
    this.stateServiceService.answers$.subscribe((answers) => {
      this.aiAnswers = answers;
    });
    this.stateServiceService.providers$.subscribe((providers) => {
      this.providers = providers;
    });

    if (this.providers.length === 0) {
      var providers = await this.n305RestService.providers();
      this.stateServiceService.saveProviders(providers);
      this.providers = providers;
    }
    if (this.providers.length > 0) {
      this.selectedProvider = this.providers[0];
    }
    this.hasLoaded = true;
  }

  ngAfterViewInit() {
    this.stateServiceService.scrollPosition$.subscribe((position) => {
      let elm = this.elRef.nativeElement.querySelector(this.answerElm);
      elm.scrollTo(0, position);
    });
  }

  ngOnDestroy() {
    let elm = this.elRef.nativeElement.querySelector(this.answerElm);
    this.stateServiceService.saveScrollPosition(elm.scrollTop);
  }

  /**
   * The method that calls that starts the AI answer process.
   * @param question 
   * @param selectedProvider 
   */
  public async sendQuestion(question: string, selectedProvider: AiProvider): Promise<void> {
    if (question && question.trim().length > 0) {
      this.disableButton = true;
      question = question.trim();
      try {
        let data = await this.n305RestService.aiAnswer(question, selectedProvider.value);
        if (data) {
          this.aiReponse = markdownToTxt(JSON.parse(JSON.stringify(data)));
        }
      } catch (error) {
        let errorMsg = `${error['statusText']}, please try again or select a different provider`.toLowerCase();
        this.aiReponse = this.capitalizeFirstLetter(errorMsg);
      }

      this.aiAnswers.push({ provider: selectedProvider.viewValue, prompt: question, answer: this.aiReponse });
      this.stateServiceService.saveAnswers(this.aiAnswers);
      this.changeDetector.detectChanges();
      this.focusOnLatestAnswer();
      this.disableButton = false;
    }
  }

  /**
   * The method that initiates deleting the app state.
   */
  public clear(): void {
    this.stateServiceService.clearState();
    this.aiAnswers = [];
  }

  /**
   * The method that capitalizes the first letter of a string.
   * @param value 
   * @returns A string.
   */
  private capitalizeFirstLetter(value: string): string {
    return `${value.charAt(0).toUpperCase()}${value.slice(1)}`;
  }

  /**
   * The method that move the scroll position to the bottom of view.
   */
  private focusOnLatestAnswer(): void {
    let elm = this.elRef.nativeElement.querySelector(this.answerElm);
    elm.scrollTo(0, elm.scrollHeight);
  }
}
